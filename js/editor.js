$(function() {
  var editor = $('#edit-jstool-textarea-js-value').codemirror({
    mode: 'javascript',
    lineNumbers: true
  });
  $('#edit-jstool-textarea-js-format').remove();
});
